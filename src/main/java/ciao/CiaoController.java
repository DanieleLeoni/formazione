package ciao;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CiaoController {
	private static final String template = "Ciao,%s!";
	private final AtomicLong counter = new AtomicLong();
	
	@RequestMapping("/ciao")
	public Ciao ciao(@RequestParam(value="name", defaultValue="World")String name)
	{
		return new Ciao(counter.incrementAndGet(),String.format(template, name));
	}
}
